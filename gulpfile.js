var loadGulp = require('./gulpfile_core.js');
loadGulp({jsLibs:[
	'craft/src/bs-assets/javascripts/bootstrap.min.js',
	'craft/src/js/vendor/jquery.validate.min.js'
],jsMain:[
	'craft/src/js/StoreMap.class.js',
	'craft/src/js/map.js',
	'craft/src/js/template.js'
],scss_styles:[
	'craft/src/scss/styles.scss'
]});