<?php
namespace Craft;
class CommonToolsPlugin extends BasePlugin
{
    public function getName()
    {
         return Craft::t('Common Tools');
    }
    public function getVersion()
    {
        return '1.0.15';
    }
    public function getDeveloper()
    {
        return 'Walker Designs';
    }
    public function getDeveloperUrl()
    {
        return 'http://walkerdesigns.com.au';
    }
    public function hasCpSection()
    {
        return false;
    }
    public function addTwigExtension()  
	{
		Craft::import('plugins.commontools.twigextensions.TimedAssetTwigExtension');
		Craft::import('plugins.commontools.twigextensions.GetEntryTwigExtension');
        Craft::import('plugins.commontools.twigextensions.StringCutTwigExtension');
        Craft::import('plugins.commontools.twigextensions.SearchStringHighlightTwigExtension');
		return array(
			new TimedAssetTwigExtension(),
            new GetEntryTwigExtension(),
            new StringCutTwigExtension(),
            new SearchStringHighlightTwigExtension(),
		);
	}
    public function registerSiteRoutes()
    {
        return array(
            'sitemap.xml' => array('action' => 'commonTools/sitemap/index'),
            'robots.txt' => array('action' => 'commonTools/robots/index'),
        );
    }
}