<?php

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class GetEntryTwigExtension extends \Twig_Extension {

	public function getName() {
		return 'GetEntry';
	}
	
	public function getFunctions() {
		return array(
			new \Twig_SimpleFunction('getEntry', array($this, 'getEntry')),
		);
	}

	public function getEntry($id) {
		return craft()->entries->getEntryById($id);
	}

}
