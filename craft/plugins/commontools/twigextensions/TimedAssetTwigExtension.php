<?php 
namespace Craft;
use Twig_Extension;
use Twig_Filter_Method;
class TimedAssetTwigExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'TimedAsset';
    }
    public function getFilters()
    {
        return array(
            'timedAsset' => new Twig_Filter_Method($this, 'timedAsset'),
        );
    }
    public function timedAsset($file)
    {	
        return $file.'?mt='.filemtime($_SERVER['DOCUMENT_ROOT'].$file);
    }
}