<?php
//searchtext|searchStringHighlight(query)|raw
namespace Craft;
use Twig_Extension;
use Twig_Filter_Method;
class SearchStringHighlightTwigExtension extends \Twig_Extension
{
	public function getName()
	{
		return 'SearchStringHighlight';
	}
	public function getFilters()
	{
		return array(
			'searchStringHighlight' => new Twig_Filter_Method($this, 'searchStringHighlight'),
		);
	}
	public function excerpt($text, $phrase, $radius = 100, $ending = "...") {
		$phraseLen = strlen($phrase);
		if ($radius < $phraseLen) {
			$radius = $phraseLen;
		}
		$pos = false;
		$phrases = explode (' ',$phrase);
		
		
		if(strlen($phrase)>0){ 
			foreach ($phrases as $phrase) {
				$pos = strpos(strtolower($text), strtolower($phrase));
				if ($pos > -1) break;
			}
		}

		$startPos = 0;
		if ($pos > $radius) {
			$startPos = $pos - $radius;
		}

		$textLen = strlen($text);

		$endPos = $pos + $phraseLen + $radius;
		if ($endPos >= $textLen) {
			$endPos = $textLen;
		}

		$excerpt = substr($text, $startPos, $endPos - $startPos);
		if ($startPos != 0) {
			$excerpt = substr_replace($excerpt, $ending, 0, $phraseLen);
		}

		if ($endPos != $textLen) {
			$excerpt = substr_replace($excerpt, $ending, -$phraseLen);
		}

		return $excerpt;
	}
	function highlight($c,$q){
		$q=explode(' ',str_replace(array('','\\','+','*','?','[','^',']','$','(',')','{','}','=','!','<','>','|',':','#','-','_'),'',$q));
		for($i=0;$i<sizeOf($q);$i++)
			$c=preg_replace("/($q[$i])(?![^<]*>)/i","<span class=\"highlight\"><strong>\${1}</strong></span>",$c);
		return $c;
	}
	public function searchStringHighlight($text,$phrase)
	{
		return $this->highlight($this->excerpt($text,$phrase,250),$phrase);
	}
}