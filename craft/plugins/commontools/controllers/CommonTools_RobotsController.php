<?php namespace Craft;
class CommonTools_RobotsController extends BaseController
{
	protected $allowAnonymous = array('actionIndex');

	public function actionIndex()
	{
		$s_siteUrl = craft()->config->get('siteUrl');
		if(craft()->config->get('devMode')===true){
			$s_robots =
'User-agent: *
Disallow: /
';
		}else{
			$s_robots =
'User-agent: *
Disallow:

' . $s_siteUrl['en'] . 'sitemap.xml
';
		}
		HeaderHelper::setContentTypeByExtension('txt');
		ob_start();
		echo $s_robots;
		craft()->end();
	}
}
