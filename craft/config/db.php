<?php
/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */
return array(
    '*' => array(
        'user' => 'rainbowl_craft',
        'password' => 'WpRBF87eARKzd',
        'database' => 'rainbowl_craft',
    ),
    //Walker Dev
    'rainbowlodge.dev.walkerdesigns.com.au' => array(
        'user' => 'root',
        'password' => 'root',
        'database' => 'rainbowlodge_craft',
    ),
    //Walker Office
    'rainbowlodge.office.walkerdesigns.com.au' => array(
        'user' => 'root',
        'password' => 'root',
        'database' => 'rainbowlodge_craft',
    ),
);