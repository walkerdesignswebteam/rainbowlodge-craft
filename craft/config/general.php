<?php
// Ensure our urls have the right scheme
define('URI_SCHEME',  ( isset($_SERVER['HTTPS'] ) ) ? "https://" : "http://" );
define('SITE_URL',    URI_SCHEME . $_SERVER['SERVER_NAME'] . '/');


/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */
return array(
    '*' => array(
        'siteName' => array(
        'en' => 'Rainbow Lodge Tasmania',
	    'ja' => 'Rainbow Lodge Tasmania', //Probably should get translated
        ),
	'siteUrl' => array(
            'en' => SITE_URL,
            'ja' => SITE_URL.'ja/',
        ),
	'setPasswordSuccessPath' => array(
            'en' => 'users/password-reset-complete',
        ),
        'setPasswordPath' => array(
            'en' => 'users/password-set',
        ),
        'defaultSearchTermOptions' => array(
            'subLeft' => true,
            'subRight' => true,
        ),
        //'googleAnalyticsId'=>'UA-00000000-0',
        'cpTrigger' => 'craftadmin',
        'imageDriver'=>'gd',
        'loginPath' => 'users/login',
        'logoutPath' => 'users/logout',
        'timezone' => 'Australia/Hobart',
        'omitScriptNameInUrls' => true, //sometimes index.php ends up in urls for no reason it seems.. this forces it to use .htaccess
    ),
    'rainbowlodge.dev.walkerdesigns.com.au' => array(
        'devMode' => true,
    ),
    'rainbowlodge.office.walkerdesigns.com.au' => array(
        'devMode' => true,

    ),
);

