/*
//PHP Example
if(isset($a_markerInfo['geocodeGdaLatitude'])){
    $a_mapArray=array(array(
        'id'=>1,
        'title'=>$a_productInfo['productName'],
        'latitude'=>$a_markerInfo['geocodeGdaLatitude'],
        'longitude'=>$a_markerInfo['geocodeGdaLongitude'],
        'iconfile'=>'default',
        'info'=>'<center><strong>'.htmlspecialchars($a_productInfo['productName']).'</strong><br />'.htmlspecialchars($s_address).'</center>',
    ));
    $s_ret.='
    <div class="googleMap" id="googleMap2015"></div>
    <script type="text/javascript">
    $(document).ready(function(){
        //Google Maps Script
        //var typeData = [{"title":"Filter by Location","data":[{"key":"cairns","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> South of Cairns","selected":0},{"key":"babinda","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Babinda","selected":0},{"key":"innisfail","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Innisfail","selected":0},{"key":"canecutter-way","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Canecutter Way","selected":0},{"key":"kurrimine-beach","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Kurrimine Beach","selected":0},{"key":"mission-beach","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Mission Beach","selected":0},{"key":"tully","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Tully","selected":0},{"key":"cardwell","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Cardwell","selected":0},{"key":"ingham","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> Around Ingham","selected":0},{"key":"townsville","html":"<i class=\"fa fa-arrow-circle-right\"><\/i> North of Townsville","selected":0}],"key":"location"},{"title":"Filter by Category","data":[{"key":"Information","html":"Information","selected":0},{"key":"Attractions","html":"Attractions","selected":1},{"key":"Accommodation","html":"Accommodation","selected":0},{"key":"Events","html":"Events","selected":0},{"key":"Food","html":"Food","selected":0}],"key":"category"}];
        var mapData = '.json_encode($a_mapArray).';
        var ceGoogleMap = new GoogleMapHandler();

        //ceGoogleMap.init(mapData,typeData,"googleMap2015","googleMapTypeFilters");
        ceGoogleMap.init(mapData,false,"googleMap2015",false);

        //Override default lat/lon if not Tasmania (e.g. Topical Coast)
        ceGoogleMap.defaultLatLon = {"lat": -18.498198, "lon": 145.944409};
        ceGoogleMap.refresh(); //force refresh becuase we set lat/lon after init
        
    });	
    </script>
    ';
}
*/
function GoogleMapHandler() {
    this.googleMapObj = null;
    this.filterContainerObj = null;
    this.googleMapMarker = null;
    this.geocoder = new google.maps.Geocoder();
    this.defaultLatLon = null;
    this.googleMapMarkers = [];
    this.googleMapInfoWindow = null;
    this.mapData = null;
    this.filterData = null;
    this.mapCurrentFilters = [];
    this.$_resultsFound = $('<div class="resultsFound"></div>');
}

GoogleMapHandler.prototype.listenerGetFilters = function(){
    var thisLoc;
    var i;
    var a_ret = [];
    for (i=0; i<this.mapData.length; i++) {
        thisLoc = this.mapData[i];
        //if this item should be fintered out, continue here
        if(this.isFiltered(thisLoc,this.mapCurrentFilters)){
            continue;
        }else{
            a_ret.push(thisLoc);
        }
    }
    return a_ret;
};

GoogleMapHandler.prototype.buildFilterUI = function(){
	function makeClickHandler(item){
		item.click(function(){
			if($(this).data('active')===true){
				$(this).data('active',false);
				$(this).removeClass('active');
			}else{
				$(this).data('active',true);
				$(this).addClass('active');
			}
			_self.refreshCurrentFilters();
			_self.update();
			_self.filterContainerObj.trigger("filtersupdated",[_self.listenerGetFilters()]);
		});
	}
    if(this.filterData){
        var _self = this;
        var i; var i2;
        var data; var key; var title;

        var $_list; var $_listitem; var $_wrapper; var $_resultsFound;
		
        for(i=0;i<this.filterData.length;i++){
            data = this.filterData[i].data;
            key = this.filterData[i].key;
            title = this.filterData[i].title;

            $_list = $('<ul class="controlList" />');
            for(i2=0;i2<data.length;i2++){
                $_listitem = $('<li class="controlItem '+data[i2].key+'"><a href="javascript:;"><span>'+data[i2].html+'</span></a></li>');
                $_listitem.data('iteminfo',{key:key,val:data[i2].key});
                if(data[i2].selected===1){
                    $_listitem.data('active',true);
                    $_listitem.addClass('active');
                }else{
                    $_listitem.data('active',false);
                }
				makeClickHandler($_listitem);
                
                $_list.append($_listitem);
            }
            
            $_wrapper = $('<div class="controlListWrap '+key+'" />');
            
            $_wrapper.append('<div class="controlListHeading">'+title+'</div>');
            $_wrapper.append($_list);
            this.filterContainerObj.append($_wrapper);
            this.filterContainerObj.append(this.$_resultsFound);
        }
        //give filters a refesh just in case some were marked as active.
        _self.refreshCurrentFilters();
    }
};


GoogleMapHandler.prototype.refreshCurrentFilters = function(){
    var _self = this;
    this.mapCurrentFilters=[];
    
    var selected;
    this.filterContainerObj.find('.controlList').each(function(){
        selected = [];
        $(this).find('.controlItem').each(function(){
            if($(this).data('active')===true){
                selected.push($(this).data('iteminfo'));
            }
        });
        if(selected.length>0){
            _self.mapCurrentFilters.push(selected);
        }
    });
};


GoogleMapHandler.prototype.escapeHtml = function (text) {
    if (text && text.length > 0) {
        return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    }
};
GoogleMapHandler.prototype.selectMarkerByRef = function (marker) {
    marker.setOptions({zIndex: marker.classRef.getHighestMarkerZIndex() + 1});
    marker.classRef.openMarkerOverlay(marker.winInfo);
};
GoogleMapHandler.prototype.getMarker = function (selectId) {
    var marker;
    for (i=0; i < this.googleMapMarkers.length; i++) {
        marker = this.googleMapMarkers[i];
        if (marker.id===selectId) {
            return marker;
        }
    }
    return false;
};

GoogleMapHandler.prototype.isFiltered = function(loc,filters) {
    var i2; var i3;
    var filterCat; var filter;
    var retResult = false;
    var foundCount = 0;
    for(i2=0;i2<filters.length;i2++){
        filterCat = filters[i2];
        retResult = true;
        for(i3=0;i3<filterCat.length;i3++){
            filter = filterCat[i3];
            if(typeof loc[filter.key] === 'string') {
                //single value
                if(filter.val===loc[filter.key]){
                    retResult = false;
                }
            }else{
                //array
                var locCnt;
                for(locCnt=0;locCnt<loc[filter.key].length;locCnt++){
                    if(filter.val===loc[filter.key][locCnt]){
                        retResult = false;
                    }
                }
            }
        }
        if(retResult===false){
            foundCount++;
        }
    }
    if(foundCount!==filters.length){
        retResult = true;
    }
    return retResult;
};
GoogleMapHandler.prototype.multiMarkerAdd = function(locations, filters) {
    var _self = this;
    var infowindow = new google.maps.InfoWindow();
    var markerBounds = new google.maps.LatLngBounds();
    var thisLoc;
    var marker;
    var countMarkersAdded = 0;
	var latLonPoint;
    var i;

	function makeClickHandler(marker){
		google.maps.event.addListener(marker, "click", function () {
            _self.selectMarkerByRef(this);
        });
	}
    this.googleMapInfoWindow.close();

    //remove any existing markers
    for (i = 0; i < this.googleMapMarkers.length; i++) {
        this.googleMapMarkers[i].setMap(null);
    }
    this.googleMapMarkers = [];
    
    for (i = 0; i < locations.length; i++) {
        thisLoc = locations[i];
        
        //if this item should be fintered out, continue here
        if(_self.isFiltered(thisLoc,filters)){
            continue;
        }

        //check to see if we should show this item based on the filters
        for(i2=0;i2<filters.length;i2++){
            filterCat = filters[i2];
            for(i3=0;i3<filterCat.length;i3++){
                filter = filterCat[i3];
            }
        }
        
        
        var iconImgPath = '/template/images/googlemap/markers/'+thisLoc.iconfile+'.png';
        
        //Set this to the pixels to move to from the top right
        var pointerPixel = {left:8,top:24};
        
        var pinImage = new google.maps.MarkerImage(iconImgPath,
            new google.maps.Size(38, 50),
            new google.maps.Point(0, 0),
            new google.maps.Point(pointerPixel.left, pointerPixel.top) //horiz/vert position
        );
 
        //create marker
        var markerZndex = 100;
        if(!thisLoc.latitude){
            console.log('No latitude for map object');
            console.log(thisLoc);
	    break;
        }
        latLonPoint = new google.maps.LatLng(thisLoc.latitude, thisLoc.longitude);
        marker = new google.maps.Marker({
            position: latLonPoint,
            icon: pinImage,
            map: this.googleMapObj,
            zIndex: 100 + i
        });
        
        marker.winInfo = {};
        marker.winInfo.location = thisLoc;
        marker.classRef = this;
        marker.id = thisLoc.id;
		
		makeClickHandler(marker);
        
        //auto select the marker if specified by defaults
        /*
        if(thisLoc.id==googleMapDefaults.autoselect){
            openMarkerOverlay(marker.winInfo);
            markerHighlighted=latLonPoint;
        }
        */

        //store marker in glogal array so we can remove em and refresh em later
        markerBounds.extend(latLonPoint);
        this.googleMapMarkers.push(marker);
        countMarkersAdded++;
    }
    
    this.$_resultsFound.html('<span class="numWrap">'+countMarkersAdded+'</span> location'+(countMarkersAdded!==1?'s':'')+' found');
    
    if (this.googleMapMarkers.length === 1) {
        _self.selectMarkerByRef(marker);
    }

    var map = this.googleMapObj;

    var b_initSet = false;
    google.maps.event.addListener(map, 'zoom_changed', function () {
        setTimeout(function(){
            if (b_initSet === false) {
                var zoomlevel = map.getZoom();
                if (zoomlevel > 16) {
                    map.setZoom(16);
                }
            }
            b_initSet = true;
        },200);
    });

    if (this.googleMapMarkers.length > 0) {
        this.googleMapObj.setCenter(markerBounds.getCenter());
        this.googleMapObj.fitBounds(markerBounds);
    } else {
        latLonPoint = new google.maps.LatLng(this.defaultLatLon.lat, this.defaultLatLon.lon);
        this.googleMapObj.setCenter(latLonPoint);
        this.googleMapObj.setZoom(7);
    }
};

GoogleMapHandler.prototype.getHighestMarkerZIndex = function () {
    var highestZIndex = 0;
    var tempZIndex;
    var i;
    if (this.googleMapMarkers.length > 0) {
        for (i = 0; i < this.googleMapMarkers.length; i++) {
            tempZIndex = this.googleMapMarkers[i].getZIndex();
            if (tempZIndex > highestZIndex) {
                highestZIndex = tempZIndex;
            }
        }
    }
    return highestZIndex;
};

GoogleMapHandler.prototype.openMarkerOverlay = function (winInfo) {
    var position = new google.maps.LatLng(winInfo.location.latitude, winInfo.location.longitude);
    var addr;
    if (winInfo.location.address_formatted) {
        addr = winInfo.location.address_formatted + ', Launceston, Tas, 7250';
    } else {
        addr = 'Launceston, Tas, 7250'; 
    }
    this.googleMapInfoWindow.open(position, '<div class="winOffset"> <div class="arrow"><div class="inner"></div></div> <div class="info">' + winInfo.location.info + '</div></div>');
};

GoogleMapHandler.prototype.updateMarker = function (latlon, autoCenterAndZoom) {
    var newLatLng = new google.maps.LatLng(latlon.lat, latlon.lon);
    this.googleMapMarker.setPosition(newLatLng);
    if (autoCenterAndZoom === true) {
        this.googleMapObj.setCenter(newLatLng);
        this.googleMapObj.setZoom(13);
    }
};
GoogleMapHandler.prototype.getCurrLatLon = function () {
    return this.googleMapMarker.position;
};
GoogleMapHandler.prototype.setMapCenter = function (zoom) {
    this.googleMapObj.setCenter(new google.maps.LatLng(this.defaultLatLon.lat, this.defaultLatLon.lon));
    this.googleMapObj.setZoom(zoom);
};

GoogleMapHandler.prototype.placeMarker = function (draggable, zoom) {
    this.setMapCenter(zoom);

    var iconImgPath = '/template/images/googlemap/markers/default.png';
    var pinImage = new google.maps.MarkerImage(iconImgPath,
        new google.maps.Size(38, 50),
        new google.maps.Point(0, 0),
        new google.maps.Point(18, 49) //horiz/vert position
    );
    if (this.googleMapMarker) {
        this.googleMapMarker.setMap(null);
    }
    var latLonPoint = new google.maps.LatLng(this.defaultLatLon.lat, this.defaultLatLon.lon);
    this.googleMapMarker = new google.maps.Marker({
        position: latLonPoint,
        icon: pinImage,
        map: this.googleMapObj,
        draggable: draggable
    });
};

GoogleMapHandler.prototype.dialogMapInfoContent = function (title, content) {
    var dialogDiv = $("#mapDialogInfo");
    dialogDiv.dialog('option', 'title', title);
    $("#mapDialogInfo .content").html(content);
    dialogDiv.dialog('open');
};
GoogleMapHandler.prototype.update = function() {
    this.multiMarkerAdd(this.mapData, this.mapCurrentFilters);
};
GoogleMapHandler.prototype.init = function(mapData,filterData,mapElementId,filterElementId) {
    var _self = this;
    this.mapData = mapData;
    this.filterData = filterData;
    
    var opts = {
        //mapTypeId: google.maps.MapTypeId.HYBRID,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: [{
                featureType: "poi",
                elementType: "labels",
                stylers: [{
                        visibility: "off"
                    }]
            }] 
    };
    
    this.defaultLatLon = {'lat': -42.060113, 'lon': 146.614600};

    this.googleMapObj = new google.maps.Map(document.getElementById(mapElementId), opts);
    
    //set up filters
    this.filterContainerObj = $('#'+filterElementId);
    this.buildFilterUI();

    // Info window
    this.googleMapInfoWindow = new TextWindow(this.googleMapObj);
    google.maps.event.addListener(this.googleMapObj, 'click', function (event) {
        _self.googleMapInfoWindow.close();
    });
    
    this.refresh();
};
GoogleMapHandler.prototype.refresh = function() {
    google.maps.event.trigger(this.googleMapObj, 'resize');
    this.multiMarkerAdd(this.mapData,this.mapCurrentFilters);
};

if(typeof google !== 'undefined'){
	//////////////////////
	// TextWindow class //
	//////////////////////
	TextWindow = function (map) {
		this._map = map;
		this._window = null;
		this._text = null;
		this._position = null;
	};
	TextWindow.prototype = new google.maps.OverlayView();

	TextWindow.prototype.open = function (latlng, text) {
		if (this._window !== null){
			this.close();
		}

		this._text = text;
		this._position = latlng;
		this.setMap(this._map);
	};

	TextWindow.prototype.close = function () {
		this.setMap(null);
	};

	TextWindow.prototype.onAdd = function () {
		this._window = document.createElement('div');
		this._window.className = "markerWindow";
		this._window.innerHTML = this._text;

		this.getPanes().floatPane.appendChild(this._window);

		//add click listener to window that stops it being removed by the global map listener.
		google.maps.event.addDomListener(this._window, 'click', function (event) {
			event.cancelBubble = true;
		});

	};

	TextWindow.prototype.draw = function () {
		var point = this.getProjection().fromLatLngToDivPixel(this._position);
		this._window.style.top = (parseInt(point.y) - 117) + 'px';
		this._window.style.left = (parseInt(point.x) - 163) + 'px';
	};

	TextWindow.prototype.onRemove = function () {
		this._window.parentNode.removeChild(this._window);
		this._window = null;
	};
	//////////////////////
	// TextWindow class //
	//////////////////////
}