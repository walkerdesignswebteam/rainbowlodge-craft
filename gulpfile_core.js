//su root
//yum install nodejs
//yum install npm
//yum install ruby
//yum install rubygems
//gem install sass
//npm install --global gulp
//npm install gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-rename gulp-cache del --save-dev

//To debug
//DEBUG=* gulp <task>

/*
 * Inspired by https://markgoodyear.com/2014/01/getting-started-with-gulp/
 */
module.exports = function(config) {
  "use strict";
	var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minifycss = require('gulp-minify-css'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat')
	;
	//imagemin = require('gulp-imagemin'),
	//notify = require('gulp-notify'),
	//cache = require('gulp-cache'),
	//del = require('del')

	gulp.task('styles', function () {
		return sass(config.scss_styles, {style: 'expanded', lineNumbers: true})
		.pipe(autoprefixer('> 1%'))
		.pipe(gulp.dest('public_html/assets/css'))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('public_html/assets/css'))
		;
	});

	// Scripts
	gulp.task('js_libs', function () {
		return gulp.src(config.jsLibs)
		.pipe(concat('libs.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('public_html/assets/js'))
		;
	});
	gulp.task('js_template', function () {
		return gulp.src(config.jsMain)
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(concat('main.js'))
		.pipe(gulp.dest('public_html/assets/js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('public_html/assets/js'))
		;
	});

	gulp.task('default', function () {
		//main task - just run "gulp"
		gulp.watch('craft/src/scss/*.scss', ['styles']);
		gulp.watch('craft/src/js/*.js', [
			'js_libs', //makes a libs.min.js file
			'js_template' // makes a main.js file
		]);
	});
};